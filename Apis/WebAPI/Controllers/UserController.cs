﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<ApiResponse<string>> RegisterAsync([FromBody] UserRegisterModel registerObject) => await _userService.RegisterUserAsync(registerObject);

        [HttpPost]
        [AllowAnonymous]
        public async Task<ApiResponse<string>> LoginAsync([FromBody] UserLoginModel loginObject) => await _userService.LoginAsync(loginObject);

        [HttpGet]
        [Authorize(Roles = AppRole.Admin)]
        public string HelloAdminAsync() => "hello admin";

        [HttpGet]
        [Authorize(Roles = AppRole.Customer)]
        public string HelloGuest() => "hello guest";
    }
}
