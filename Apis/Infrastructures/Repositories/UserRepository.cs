﻿using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public Task<bool> CheckEmailNameExisted(string emailName) => _dbContext.Users.AnyAsync(u => u.Email == emailName);

        public Task<bool> CheckPhoneNumberExisted(string phoneNumber) => _dbContext.Users.AnyAsync(u => u.PhoneNumber == phoneNumber);

        public async Task<User> GetUserByEmailAndPassword(string email, string password)
        {
            var user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Email == email);
            if (user == null)
            {
                throw new Exception("UserName is not existed!");
            }
            string passwordHash = password.HashPassword(user.Salt);
            if (user.PasswordHash != passwordHash)
            {
                throw new Exception("Password is not correct");
            }
            return user;
        }
    }
}
