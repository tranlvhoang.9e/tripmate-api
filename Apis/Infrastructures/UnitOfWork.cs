﻿using Application;
using Application.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly IUserRepository _userRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IReviewRepository _reviewRepository;

        public UnitOfWork(AppDbContext dbContext,
            IUserRepository userRepository,
            IBookingRepository bookingRepository,
            ITransactionRepository transactionRepository,
            IReviewRepository reviewRepository)
        {
            _dbContext = dbContext;
            _userRepository = userRepository;
            _bookingRepository = bookingRepository;
            _transactionRepository = transactionRepository;
            _reviewRepository = reviewRepository;
        }

        public IUserRepository UserRepository => _userRepository;

        public ITransactionRepository TransactionRepository => _transactionRepository;

        public IReviewRepository ReviewRepository => _reviewRepository;
        public IBookingRepository BookingRepository => _bookingRepository;

        public async Task<int> SaveChangeAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
