﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using FluentValidation;
using System.Security.Cryptography;
using ValidationResult = FluentValidation.Results.ValidationResult;
using Application.Utils;

namespace Infrastructures.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;
        private readonly IValidator<UserRegisterModel> _validatorRegister;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration, IValidator<UserRegisterModel> validatorRegister)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _validatorRegister = validatorRegister;
        }

        public async Task<ApiResponse<string>> LoginAsync(UserLoginModel loginObject)
        {
            var response = new ApiResponse<string>();
            try
            {
                var user = await _unitOfWork.UserRepository.GetUserByEmailAndPassword(loginObject.Email, loginObject.Password);
                response.Data =  user.GenerateJsonWebToken(_configuration.JWTSection.SecretKey, _currentTime.GetCurrentTime(), _configuration.JWTSection.Issuer, _configuration.JWTSection.Audience, _configuration.JWTSection.ExpiresInMinutes);
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> RegisterUserAsync(UserRegisterModel registerObject)
        {
            ValidationResult validationResult = await _validatorRegister.ValidateAsync(registerObject);
            var response = new ApiResponse<string>();
            try
            {
                if (!validationResult.IsValid)
                {
                    throw new Exception(string.Join(", ", validationResult.Errors.Select(error => error.ErrorMessage)));
                }
                var isExisted = await _unitOfWork.UserRepository.CheckEmailNameExisted(registerObject.Email);
                if (isExisted)
                {
                    throw new Exception("Email existed please try again");
                }
                isExisted = await _unitOfWork.UserRepository.CheckPhoneNumberExisted(registerObject.PhoneNumber);
                if (isExisted)
                {
                    throw new Exception("Phone number existed please try again");
                }
                var newCustomer = _mapper.Map<User>(registerObject);
                newCustomer.Role = RoleEnum.Customer;

                //create salt
                byte[] randomBytes = RandomNumberGenerator.GetBytes(15);
                IEnumerable<string> saltHex = randomBytes.Select(x => x.ToString("x2"));
                string salt = string.Join("", saltHex);
                newCustomer.Salt = salt;
                newCustomer.PasswordHash = registerObject.Password.HashPassword(salt);

                await _unitOfWork.UserRepository.AddAsync(newCustomer);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    response.Message = "Resigter success"; 
                }
                else
                {
                    throw new Exception("Resigter fail! Please try again");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        //    public async Task<string> LoginAsync(UserLoginDTO userObject)
        //    {
        //        var user = await _unitOfWork.UserRepository.GetUserByUserNameAndPasswordHash(userObject.UserName, userObject.Password.Hash());
        //        return user.GenerateJsonWebToken(_configuration.JWTSecretKey, _currentTime.GetCurrentTime());
        //    }

        //    public async Task RegisterAsync(UserLoginDTO userObject)
        //    {
        //        // check username exited
        //        var isExited = await _unitOfWork.UserRepository.CheckUserNameExited(userObject.UserName);

        //        if (isExited)
        //        {
        //            throw new Exception("Username exited please try again");
        //        }

        //        var newUser = new User
        //        {
        //            UserName = userObject.UserName,
        //            PasswordHash = userObject.Password.Hash()
        //        };

        //        await _unitOfWork.UserRepository.AddAsync(newUser);
        //        await _unitOfWork.SaveChangeAsync();
        //    }
        //}
    }
}
