﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commons
{
    public static class AppRole
    {
        public const string Admin = "Admin";
        public const string Customer = "Customer";
        public const string Guide = "Guide";
    }
}
