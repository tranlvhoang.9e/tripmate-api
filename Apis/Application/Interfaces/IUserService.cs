﻿using Application.Commons;
using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserService
    {
        public Task<ApiResponse<string>> LoginAsync(UserLoginModel loginObject);
        public Task<ApiResponse<string>> RegisterUserAsync(UserRegisterModel registerObject);
    }
}
