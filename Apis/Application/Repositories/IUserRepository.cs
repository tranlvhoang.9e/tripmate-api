﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<bool> CheckEmailNameExisted(string emailName);
        Task<bool> CheckPhoneNumberExisted(string phoneNumber);
        Task<User> GetUserByEmailAndPassword(string email, string password);
    }
}
