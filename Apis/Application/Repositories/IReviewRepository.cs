﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
    }
}
