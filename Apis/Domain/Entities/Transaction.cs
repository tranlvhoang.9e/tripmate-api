﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Transaction : BaseEntity
    {
        public int Amount { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public TransactionTypeEnum TransactionType { get; set; }
        public StatusTransactionEnum Status { get; set; }
        public Guid? UserId { get; set; }
        public Guid? BookingId { get; set; }
        public virtual Booking Booking { get; set; }
        public virtual User User { get; set; }
    }
}
