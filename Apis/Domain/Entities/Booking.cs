﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Booking : BaseEntity
    {
        public DateTime BookingDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string Location { get; set; }
        public int TotalAmount { get; set; }
        public StatusBookingEnum Status { get; set; }
        public string? AnonymousName { get; set; }
        public string? AnonymousEmail { get; set; }
        public Guid? CustomerID { get; set; }
        public Guid GuideID { get; set; }
        public virtual User Customer { get; set; }
        public virtual User Guide { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public virtual Review Review { get; set; }    
    }
}
