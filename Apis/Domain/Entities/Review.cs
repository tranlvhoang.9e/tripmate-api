﻿namespace Domain.Entities
{
    public class Review : BaseEntity
    {
        public int Rating { get; set; }
        public string? Comment { get; set; }
        public Guid BookingID { get; set; }
        public virtual Booking Booking { get; set; }
    }
}
