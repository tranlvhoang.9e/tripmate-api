﻿using Domain.Enums;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Salt { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public RoleEnum Role { get; set; }
        public string? ImageURL { get; set; }
        public string? Bio { get; set; }
        public string? Languages { get; set; }
        public float? Rating { get; set; }
        public StatusUserEnum Status { get; set; }
        public virtual ICollection<Booking>? CustomerBookings { get; set; }
        public virtual ICollection<Booking>? GuideBookings { get; set; }
        public virtual ICollection<Transaction>? Transactions { get; set; }
        public virtual ICollection<Review>? Reviews { get; set; }
    }
}
