﻿namespace Domain.Enums
{
    public enum StatusTransactionEnum
    {
        Pending,
        Completed,
        Failed
    }
}
