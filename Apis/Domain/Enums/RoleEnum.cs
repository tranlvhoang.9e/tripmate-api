﻿namespace Domain.Enums
{
    public enum RoleEnum
    {
        Customer = 1,
        Guide = 2,
        Admin = 3
    }
}
