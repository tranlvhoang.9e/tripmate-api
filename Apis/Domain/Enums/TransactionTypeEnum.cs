﻿namespace Domain.Enums
{
    public enum TransactionTypeEnum
    {
        PlatformFee,
        CustomerPayment,
        GuidePayment,
    }
}
